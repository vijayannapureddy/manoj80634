package com.practice;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Sum {

	public int luckySum(int a, int b, int c){

		List<Integer>  numbers=Arrays.asList(a,b,c);

		int result=0;

		for(int i:numbers){

			if(i!=13){
				result+=i;
			}else{
				break;
			}
		}

		return result;
	}
}
