package com.practice;

import org.junit.Assert;
import org.junit.Test;

public class SumTest {

	Sum sut=new Sum();
	
	@Test
	public void luckySum_test1(){
		
		int exepctedResult=6;
		
		int actualResult=sut.luckySum(1, 2, 3) ;
		
		Assert.assertEquals(exepctedResult, actualResult);
	}
	
	@Test
	public void luckySum_test2(){
		
		int exepctedResult=3;
		
		int actualResult=sut.luckySum(1, 2, 13) ;
		
		Assert.assertEquals(exepctedResult, actualResult);
	}
	
	@Test
	public void luckySum_test3(){
		
		int exepctedResult=1;
		
		int actualResult=sut.luckySum(1, 13, 3) ;
		
		Assert.assertEquals(exepctedResult, actualResult);
	}
}

